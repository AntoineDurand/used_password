from cx_Freeze import setup, Executable

setup(
    name="Programme",
    version="1.0",
    author="Antoine Durand",
    author_email="durandantoine.dev.pro@gmail.com",
    description="executable",
    executables=[Executable("app.py")],
)
from cx_Freeze import setup, Executable
import os.path

PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
os.environ['TCL_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
os.environ['TK_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')

base = "Win32GUI"

options = {
    'build_exe': {
        'include_files':[
            os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tk86t.dll'),
            os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tcl86t.dll'),
         ],
    },
}

setup(name = "Vérification mot de passe",
  options = options,
  version = "0.0.0.1",
  author="Antoine DURAND",
  author_email="durandantoine.dev.pro@gmail.com",
  description = "Verifie que le mot de passe est sécurisé",
  executables=[Executable('app.py',base=base, icon="icone.png")]
)