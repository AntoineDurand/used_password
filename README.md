# ![center](logo.png)

> Projet Mot de passes

Permet de savoir si son mot de passe est fréquement utilisé

## Ennoncé

1. fonction lecture du fichier des mots de passe avec suppression du retour chariot
2. rechercher une ligne dans la liste des mots de passe
3. récupérer l'indice d'un élément de la liste
4. faire l'interface graphique

## Comment sa marche ?

```bash
python app.py
```

## Purpose

Projet a rendre

## Auteur

[Antoine DURAND](https://www.gendarmerie.interieur.gouv.fr/), Thomas BOYER, Thomas DEGEUSE, Alexis BUZIN, Enzo Bouchenez