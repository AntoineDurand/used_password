from tkinter.messagebox import *
from tkinter import * 

default_sentence = "C'est la guerre ! La République croule sous les attaques de l'impitoyable Sith, le " \
    "Comte Dooku. Il y a des héros dans les deux camps. Le Mal est partout. " \
    "Avec une audace stupéfiante, le Général Grievous, diabolique chef droïde, est entré " \
    "dans la capitale pour enlever le Chancelier Palpatine, chef suprême du Sénat Galactique. " \
    "Alors que l'Armée Séparatiste Droïde tente de quitter la capitale assiégée avec son " \
    "précieux otage, deux Chevaliers Jedi mènent une mission désespérée pour secourir le " \
    "chancelier captif.... "

separator = '*************************\r\n'
separator2 = '\r\n*************************\r\n'

color_blue = "#2980b9"
color_lightblue = "#3498db"
color_white = "#ecf0f1"
color_black = "#2c3e50"
color_grey = "#7f8c8d"
color_lightgrey = "#95a5a6"
color_red = "#c0392b"
color_lightred = "#e74c3c"

def open_file():
    #Ouverture du fichier text
    with open('imports/10k_mdp.txt', 'r') as file:
        lines =  file.readlines()
        lines = [line.lower() for line in lines]
        lines = [line.strip() for line in lines]

    return lines

def get_textarea_content():
    return textarea.get("0.0", END).strip()

def check_textarea_content():
    string = get_textarea_content()
    if len(string) == 0:
        showwarning(title="Erreur", message="Veuillez saissir un texte !")
        return False
    return True

def callback_along():
    line_number = 0
    list_of_results = []
    if check_textarea_content():
        user_pass=get_textarea_content()
        if var_btn_along.get() == "Chercher along":
            var_btn_along.set('Effacer le texte')
            var_btn_strict.set('Effacer le texte')
            for line in lines:
                line_number += 1
                if user_pass in line:
                    list_of_results.append((line_number, line.rstrip()))
                    textarea.delete('1.0', END)
                    if list_of_results:
                        for elem in list_of_results:
                            ligne_nb = "Ligne:%s" % elem[0]
                            ligne_password = "\r\nPassword: %s%s" % (elem[1], separator2)
                            textarea.insert(INSERT, ligne_nb)
                            textarea.insert(INSERT, ligne_password)
                    else:
                        showinfo(title="Mot de passe sécurisé",
                                    message="\r\nLe mot de passe %s ne figure pas dans la liste " \
                                        "des 10.000 mots de passe les plus vulnérable" % (user_pass))           
        elif var_btn_along.get() == "Effacer le texte":
            var_btn_along.set("Chercher along")
            var_btn_strict.set("Chercher strict")
            textarea.delete('1.0', END)
                
def callback_strict():
    line_number = 0
    if check_textarea_content():
        user_pass=get_textarea_content()
        if var_btn_strict.get() == "Chercher strict":
            var_btn_strict.set("Effacer le texte")
            var_btn_along.set('Effacer le texte')
            if user_pass in lines:
                textarea.delete('1.0', END)
                line_number  = lines.index(user_pass)
                ligne_nb = "%sLigne:%s" % (separator, line_number)
                ligne_password = "\r\nPassword: %s%s" % (user_pass, separator)
                textarea.insert(INSERT, ligne_nb)
                textarea.insert(INSERT, ligne_password)
            else:
                showinfo(title="Mot de passe sécurisé",
                            message="\r\nLe mot de passe %s ne figure pas dans la liste " \
                                "des 10.000 mots de passe les plus vulnérable" % (user_pass))
        elif var_btn_strict.get() == "Effacer le texte":
            var_btn_strict.set("Chercher strict")
            var_btn_along.set("Chercher along")
            textarea.delete('1.0', END)

def callback_about(event):
    showinfo(title="A propos",
             message="Application faite par Antoine DURAND")

lines=open_file()
frame = Tk()
frame.title("World greatest Password tester")
var_btn_along=StringVar(frame)
var_btn_strict=StringVar(frame)
var_btn_along.set("Chercher along")
var_btn_strict.set("Chercher strict")
icon = PhotoImage(file='icon.png')
frame.iconphoto(False, icon)
frame.minsize(500, 450)

frame.rowconfigure(1, weight=1)
frame.columnconfigure(0, weight=1)
frame.columnconfigure(1, weight=1)
frame.columnconfigure(2, weight=1)

menu = Menu(frame)
file_menu = Menu(menu, tearoff=0)
file_menu.add_command(label='Chercher along', command=callback_along)
file_menu.add_command(label='Chercher strict', command=callback_strict)
file_menu.add_separator()
file_menu.add_command(label='Quitter', command=frame.quit)
menu.add_cascade(label='MENU', menu=file_menu)
frame.config(menu=menu)

logobanner = Canvas(frame,
                    width=300,
                    height=150,
                    bd=0,
                    highlightthickness=0,
                    relief='ridge')
logobanner.bind("<Button-1>", callback_about)
banner = PhotoImage(file='logo.png')
logobanner.grid(row=0, column=1)
logobanner.create_image(0, 0, image=banner, anchor='nw')

textarea = Text(frame,
                wrap='word',
                bg=color_white,
                fg=color_black)
textarea.insert(END, default_sentence)
textarea.grid(row=1, columnspan=3, padx=10, sticky='nsew')

btn_search_along = Button(frame,
                            compound="left",
                            textvariable=var_btn_along,
                            command=callback_along,
                            bg=color_blue,
                            activebackground=color_lightblue)
btn_search_along.grid(row=2, column=0, padx=10, pady=10)

btn_search_strict = Button(frame,
                            compound="left",
                            textvariable=var_btn_strict,
                            command=callback_strict,
                            bg=color_grey,
                            activebackground=color_lightgrey,
                            fg=color_black,
                            activeforeground=color_black)
btn_search_strict.grid(row=2, column=1, padx=10, pady=10)

btn_quit = Button(frame,
                    text="Exit",
                    command=frame.quit,
                    bg=color_red,
                    activebackground=color_lightred,
                    fg=color_black,
                    activeforeground=color_black)
btn_quit.grid(row=2, column=2)

frame.mainloop()